/**
 * The examples provided by Oculus are for non-commercial testing and
 * evaluation purposes only.
 *
 * Oculus reserves all rights not expressly granted.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL
 * OCULUS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

'use strict';

/**
 * LayoutSample displays a list of buttons arranged vertically based on the flexbox
 * layout algorithm that is built into a React <View>. Layout automatically applies to all
 * view children, automatically computing their placement.
 */

import React from 'react';
import {
    asset,
    AppRegistry,
    Pano,
    Text,
    View,
    Image,
    Animated,
    Box,
    Cylinder,
    VrButton,
    NativeModules,
} from 'react-vr';
//import SoundShape from './components/SoundShape';

/**
 * HighlightView implements a custom "button" behaviour by changing the background color
 * of a nested view in response to onEnter/onExit events. These events are fired whenever
 * mouse cursor of VR gaze enters the view.
 * HighlightView makes use of two properties:
 * text            - used to specify the string to display
 * backgroundColor - color to use for background when not highlighted
 */

class LayoutSample extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            RotateValue: new Animated.Value(0),
        };

        let rotation = 0;
        let rotationHand = () => {
            rotation = rotation + 5;
            Animated.sequence([
                Animated.timing(
                    this.state.RotateValue,
                    {
                        toValue: rotation,
                        duration: 1
                    }
                )
            ]).start(() => {
                rotationHand();
            })
        };

        rotationHand();
    }

    render() {
        // <View> below creates a view that is 2 meters wide and is positioned 5 meters in front
        // of the user (z = -5). Its child items are laid out in a 'column' and marked to 'stretch'
        // to the width of the view container. This causes call child view to have the same width.
        return (
            <View>
                <Pano source={asset('classroom3.jpg')}/>
            </View>)
    }
}

AppRegistry.registerComponent('LayoutSample', () => LayoutSample);