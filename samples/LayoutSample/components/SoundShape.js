import React from 'react';
import {
    VrButton,
    Animated,
} from 'react-vr';

export default class SoundShape extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            bounceValue: new Animated.Value(1),
        };
    }

    animateEnter() {
        Animated.spring(
            this.state.RotateValue,
            {
                toValue: 1,
                friction: 4,
            }
        ).start();
    }

    animateExit() {
        Animated.timing(
            this.state.RotateValue,
            {
                toValue: 0,
                duration: 50,
            }
        ).start();
    }

    render() {
        return (
            <Animated.View
                style={{
                    transform: [
                        {rotateX: this.state.RotateValue},
                    ],
                }}
            >
                <VrButton
                    onEnter={()=>this.animateEnter()}
                    onExit={()=>this.animateExit()}
                >
                    {this.props.children}
                </VrButton>
            </Animated.View>
        );
    }
};